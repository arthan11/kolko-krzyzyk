
class Pole(object):
    def __str__(self):
        return '{}()'.format(self.__class__.__name__)

    def __repr__(self):
        return self.__str__()

class Krzyzyk(Pole):
    pass

class Kolko(Pole):
    pass



class Plansza(object):
    def __init__(self):
        self.pola = [[None for i in range(3)] for i in range(3)]

    def ustaw(self, x, y, typ):
        x -= 1
        y -= 1
        self.pola[y][x] = typ()

    def show(self):
        for wiersz in self.pola:
            print '[',
            for pole in wiersz:
                if pole.__class__ == Krzyzyk:
                    print 'x',
                elif pole.__class__ == Kolko:
                    print 'o',
                else:
                    print '_',
            print ']'
        print ''

    def check_class(self, x, y, typ):
        return self.pola[y][x].__class__ == typ

    def czy_wygral(self, typ):
        for i in range(3):
            if self.check_class(i, 0, typ) and self.check_class(i, 1, typ) and self.check_class(i, 2, typ):
                return True
            if self.check_class(0, i, typ) and self.check_class(1, i, typ) and self.check_class(2, i, typ):
                return True
        if self.check_class(0, 0, typ) and self.check_class(1, 1, typ) and self.check_class(2, 2, typ):
            return True
        if self.check_class(2, 0, typ) and self.check_class(1, 1, typ) and self.check_class(0, 2, typ):
            return True
        return False

if __name__ == '__main__':
    p = Plansza()
    p.show()
    p.ustaw(2, 2, Kolko)
    p.ustaw(1, 1, Krzyzyk)
    p.ustaw(2, 1, Kolko)
    p.ustaw(2, 3, Krzyzyk)
    p.ustaw(3, 2, Kolko)
    p.ustaw(1, 3, Krzyzyk)

    p.show()
    print p.czy_wygral(Krzyzyk)
